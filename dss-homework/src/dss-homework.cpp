// dss-homework.cpp :
// 
// Author: Daniel Luthcke
//

#include "FileDownload.h"
#include "JsonProcessing.h"
#include "Rendering.h"
#include "Management.h"

#include <filesystem>

int main()
{
    std::string testFileFolder("testfiles");

    FileDownload fd;
    JsonProcessing jp;
    Rendering render;

    Management mgmt(jp, fd, render, testFileFolder);

    //Create the test images directory if it doesn't exist

    if( !std::filesystem::exists(testFileFolder))
        std::filesystem::create_directory("testfiles");

    //Startup the rendering thread
    std::thread renderThread(&Rendering::runRender, &render);

    //Startup the management thread to control rendering
    std::thread mgmtThread(&Management::run, &mgmt);

    renderThread.join();
    mgmtThread.join();

    //Cleanup temp files
    std::filesystem::remove_all(testFileFolder);

    return 0;
}