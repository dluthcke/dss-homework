#include "FileDownload.h"
#include <curl/curl.h>
#include <curl/options.h>
#include <curl/easy.h>
#include <sstream>
#include <iostream>

//TODO: these could probably be turned into lambda functions
size_t write_data_ascii(void* ptr, size_t size, size_t nmemb, void* stream)
{
    std::string data((const char*)ptr, (size_t)size * nmemb);
    *((std::stringstream*)stream) << data;
    return size * nmemb;
}

size_t write_data_binary(void* ptr, size_t size, size_t nmemb, void* stream) 
{
    size_t realsize = size * nmemb;

    std::vector<byte>& outVector = *reinterpret_cast<std::vector<byte>*>(stream);

    std::vector<byte> tempVector((const char*) ptr, (const char*) ptr + realsize );

    outVector.reserve(outVector.size() + tempVector.size());
    outVector.insert(outVector.end(), tempVector.begin(), tempVector.end());  
    return realsize;
}

size_t write_data_file(void* ptr, size_t size, size_t nmemb, FILE* stream) 
{
    size_t written = fwrite(ptr, size, nmemb, stream);
    return written;
}



std::string FileDownload::DownloadUrlToString(const std::string url)
{
    std::stringstream result;
    CURL* curl;
    CURLcode res;

    curl = curl_easy_init();
    if (curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data_ascii );
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &result);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);

        if (res != CURLE_OK)
        {
            printf("Curl failed: %s\n", curl_easy_strerror(res));
            printf("\tCouldn't download %s to string", url.c_str());
            result.clear();
        }
    }

	return result.str();
}

std::vector<char> FileDownload::DownloadUrlToBin(const std::string url)
{
    std::vector<char> result;
    CURL* curl;
    CURLcode res;

    curl = curl_easy_init();
    if (curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data_binary);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &result);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);

        if (res != CURLE_OK)
        {
            printf("Curl failed: %s\n", curl_easy_strerror(res));
            printf("\tCouldn't download %s to binary", url.c_str());
            result.clear();
        }
    }

    return result;
}

bool FileDownload::DownloadUrlToFile(const std::string url, const std::string filepath)
{
    CURL * curl;
    FILE* fp;
    CURLcode res;

    curl = curl_easy_init();
    if (curl)
    {
        fp = fopen(filepath.c_str(), "wb");
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data_file);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);

        if (res != CURLE_OK)
        {
            printf("Curl failed: %s\n", curl_easy_strerror(res));
            printf("\tCouldn't download %s to file", url.c_str());
            //Close and remove the file, it isn't valid
            fclose(fp);
            remove(filepath.c_str());
            return false;
        }

        fflush(fp);

        fclose(fp);

    }
    return true;
}
