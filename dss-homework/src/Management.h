#pragma once

#include "JsonProcessing.h"
#include "Rendering.h"
#include "FileDownload.h"

#include <semaphore>
#include <vector>

class Management
{
public:
	Management(JsonProcessing& jp, FileDownload& fd,
		Rendering& render, std::string fileFolder);

	~Management();
	void run();

	void ColIncrementHandler(const int row, const bool decrement);
	void RowIncrementHandler(const bool decrement);
	void ActionHandler(const unsigned int row, const unsigned int column);
	void BackHandler();
	void Exithandler();


private:
	bool loadVisibleTiles();

	void updateRow(const int rowIndex);

	void downloadAndDisplayTitle(const json items, const unsigned int containerIndex, const unsigned int itemIndex, const unsigned int row, const unsigned int column);

	void drawItemLandingPage(const int row, const int column);


	struct containerItemList_t
	{
		json container;
		std::vector<json> Items;
	};

	std::vector<json> m_ContainerList;
	std::binary_semaphore m_RerenderSem;
	std::string m_fileDir;
	bool m_terminateCalled;
	bool m_renderLandingPage;

	enum class HomePageItems_t
	{
		CONTAINER = 0,
		ROW_1 = 1,
		ROW_2 = 2,
		ROW_3 = 3,
		ROW_4 = 4
	};

	//Keeps track of totals
	std::map<HomePageItems_t, unsigned int> m_PageTotalsMap;
	//Keeps track of what is displayed on the screen
	std::map<HomePageItems_t, unsigned int> m_PageVisualMap;
	//Save off any ref ids we have already downloaded
	std::map<std::string, json> m_refIdMap;


	JsonProcessing& m_JsonProc;
	FileDownload& m_FileDownload;
	Rendering& m_Render;

private:
};

