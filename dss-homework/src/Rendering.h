#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_main.h>
#include <SDL2/SDL_image.h>

#include <functional>
#include <string>
#include <vector>
#include <queue>
#include <mutex>
#include <map>

class Rendering
{
public:

	using TColCallback = std::function<void(const int, const bool)>;
	using TRowCallback = std::function<void(const bool)>;
	using TBackCallback = std::function<void()>;
	using TActionCallback = std::function<void(const unsigned int, const unsigned int)>;
	using TExitCallback = std::function<void()>;

	Rendering();

	void runRender();

	//functions for home page rendering
	int createTile(const std::string imgFile, const unsigned int column, const unsigned int row);
	int createTileText(const std::string title, const unsigned int column, const unsigned int row);
	int createRowTitle(const std::string title, const unsigned int row);

	//functions for Information page
	int renderInfoPage(const std::string title, const std::string backgroundFile,
		const std::string posterFile, const std::string rating, const std::string releaseDate);

	//Add or remove arrows indicating more shows to scroll through
	int createMoreArrow(const unsigned int row);
	int createLessArrow(const unsigned int row);
	int deleteMoreArrow(const unsigned int row);
	int deleteLessArrow(const unsigned int row);

	//General functions
	void clearScreen();


	//Register callback for displalying next or previous columns
	void registerColIncrementCallback(TColCallback callback);
	//Register callback for displaying next or previous rows
	void registerRowIncrementCallback(TRowCallback callback);
	//Register callback when back button is pressed
	void registerBackCallback(TBackCallback callback);
	//Register callback when action button is pressed
	void registerActionCallback(TActionCallback callback);
	//Register callback when exiting
	void registerExitCallback(TExitCallback callback);

	void setTileWidth(const unsigned int width)
	{
		m_TileWidth = width;
	};
	void setTileHeight(const unsigned int height)
	{
		m_TileHeight = height;
	};
	bool getInitComplete()
	{
		return m_initComplete;
	}

	const unsigned int Max_Rows = 4;
	const unsigned int Max_Columns = 5;

	//An enum to help keep track of what is being displayed where.
	//should help with mutli threading.
	enum class TILE_POSITIONS
	{
		ROW_1_TITLE = 0,
		ONE_1,
		ONE_1_TEXT,
		ONE_2,
		ONE_2_TEXT,
		ONE_3,
		ONE_3_TEXT,
		ONE_4,
		ONE_4_TEXT,
		ONE_5,
		ONE_5_TEXT,
		ROW_2_TITLE,
		TWO_1,
		TWO_1_TEXT,
		TWO_2,
		TWO_2_TEXT,
		TWO_3,
		TWO_3_TEXT,
		TWO_4,
		TWO_4_TEXT,
		TWO_5,
		TWO_5_TEXT,
		ROW_3_TITLE,
		THREE_1,
		THREE_1_TEXT,
		THREE_2,
		THREE_2_TEXT,
		THREE_3,
		THREE_3_TEXT,
		THREE_4,
		THREE_4_TEXT,
		THREE_5,
		THREE_5_TEXT,
		ROW_4_TITLE,
		FOUR_1,
		FOUR_1_TEXT,
		FOUR_2,
		FOUR_2_TEXT,
		FOUR_3,
		FOUR_3_TEXT,
		FOUR_4,
		FOUR_4_TEXT,
		FOUR_5,
		FOUR_5_TEXT,
		ROW_1_LESS,
		ROW_1_MORE,
		ROW_2_LESS,
		ROW_2_MORE,
		ROW_3_LESS,
		ROW_3_MORE,
		ROW_4_LESS,
		ROW_4_MORE,
	};


private:
	TILE_POSITIONS columnRowToEnum(const int row, const int column, const bool isText = false);

	bool m_initComplete;
	SDL_Renderer* m_rend;
	SDL_Window* m_win;

	struct TileData
	{
		SDL_Texture* texture;
		SDL_Rect position;
		unsigned int columnPos;
		unsigned int rowPos;
	};

	unsigned int m_TileWidth;
	unsigned int m_TileHeight;

	TColCallback	m_columnIncCallback;
	TRowCallback	m_rowIncCallback;
	TBackCallback   m_backCallback;
	TActionCallback m_actionCallback;
	TExitCallback   m_exitCallback;

	// Use this mutex to add new tiles into the render
	// queue while we aren't actually rendering
	//TODO: probably can be done more efficiently
	std::mutex m_renderMutex;

	std::map<TILE_POSITIONS, TileData> m_tileMap;
	//Temp tile map is to populate all at once and hopefully help stop flickering
	//std::map<TILE_POSITIONS, TileData> m_tempTileMap;
};

