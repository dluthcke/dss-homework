#include "JsonProcessing.h"

#include <iostream>
#include <exception>


void to_json(json& j, const classText& text)
{
	j = json{ 
			{"content", text.content},
			{"language", text.language}, 
			{"sourceEntity", text.SourceEntity} 
		};
};
	
void from_json(const json& j, classText& text)
{
	j.at("content").get_to(text.content);
	j.at("language").get_to(text.language);
	j.at("sourceEntity").get_to(text.SourceEntity);
};


NLOHMANN_JSON_SERIALIZE_ENUM(ContainerType, {
	{ContainerType::CT_SHELF, "ShelfContainer"},
	{ContainerType::CT_CURATED_SET, "CuratedSet"},
	});

NLOHMANN_JSON_SERIALIZE_ENUM(SetType, {
	{SetType::BECAUSE_YOU, "BecauseYouSet" },
	{SetType::CURATED, "CuratedSet"},
	{SetType::TRENDING, "TrendingSet"},
	{SetType::PERSONALIZED_CURATED, "PersonalizedCuratedSet" },
	{SetType::REF, "SetRef"},
	{SetType::SERIES, "DmcSeries"},
	{SetType::VIDEO, "DmcVideo"},
	{SetType::STANDARD_COLLECTION, "StandardCollection"},
});

std::vector<json> JsonProcessing::getContainersFromHome(const std::string& data)
{

	std::vector<json> containers;
	try
	{
		auto jsonData = json::parse(data)["data"]["StandardCollection"];

		//Parse out all of the containers
		for (auto i = 0; i < jsonData["containers"].size(); i++)
		{
			containers.push_back(jsonData["containers"][i]["set"]);
		}
	}
	catch (std::exception& e)
	{
		std::cout << "getContainersFromHome Exception: " << e.what() << std::endl;
	}
	return containers;
}

std::vector<json> JsonProcessing::getItemsFromSet(const json& data)
{
	std::vector<json> items;
	try
	{
		switch (data["type"].get<SetType>())
		{
		case SetType::CURATED:
		case SetType::PERSONALIZED_CURATED:
		case SetType::TRENDING:
			//Check to make sure we have an item list before trying to parse
			if (data.contains("items"))
			{
				for (auto i = 0; i < data["items"].size(); i++)
				{
					items.push_back(data["items"][i]);
				}
			}
			else
			{
				std::cout << "getItemsFromSet error: Can't find an items list" << std::endl;
			}
			break;

		case SetType::REF:
			std::cout << "getItemsFromSet Error: refSet passed it, make sure to download the ref json first" << std::endl;
			break;

		default:
			std::cout << "getItemsFromSet Error: Unknown Set type - " << data["type"].get<std::string>() << std::endl;
			break;
		}

	}
	catch (std::exception& e)
	{
		std::cout << "getItemsFromSet Exception: " << e.what() << std::endl;
	}

	return items;
}

classText JsonProcessing::getTextFromItem(const json& data)
{
	try
	{
		switch (data["type"].get<SetType>())
		{
		case SetType::SERIES:
			return data["text"]["title"]["full"]["series"]["default"].get<classText>();

		case SetType::VIDEO:
			return data["text"]["title"]["full"]["program"]["default"].get<classText>();

		case SetType::STANDARD_COLLECTION:
			return data["text"]["title"]["full"]["collection"]["default"].get<classText>();

		case SetType::REF:
		case SetType::CURATED:
			return data["text"]["title"]["full"]["set"]["default"].get<classText>();
		}
	}
	catch (std::exception& e)
	{
		std::cout << "getTextFromItem Exception: " << e.what() << std::endl;
	}

	return classText();
}

JsonProcessing::jsonImagesType_t JsonProcessing::getImageFromItem(const json& data)
{
	jsonImagesType_t images;
	try 
	{
		// Iterate through and save all image types
		for( auto& type : data["image"].items() )
		{
			imageType_t image;

			//iterate through all image types and save off ratio with url
			for (auto& ratio : type.value().items())
			{
				std::string url;

				switch (data["type"].get<SetType>())
				{
				case SetType::VIDEO:
					url = ratio.value()["program"]["default"]["url"].get<std::string>();
					break;
				case SetType::SERIES:
					url = ratio.value()["series"]["default"]["url"].get<std::string>();
					break;
				case SetType::STANDARD_COLLECTION:
					url = ratio.value()["default"]["default"]["url"].get<std::string>();
					break;
				default:
					std::cout << "getImageFromItem Unhandled Type: " << (int)data["type"].get<SetType>() << std::endl;
				}
				image.insert(std::pair<std::string, std::string>(ratio.key(), url));
			}

			images.insert(std::pair<std::string, imageType_t>(type.key(), image));
		}
	}
	catch (std::exception& e)
	{
		std::cout << "getImageFromItem Exception: " << e.what() << std::endl;
	}
	return images;
}

std::string JsonProcessing::getContentIdFromItem(const json& data)
{
	try {
		switch (data["type"].get<SetType>())
		{
		case SetType::STANDARD_COLLECTION:
			return data["collectionId"].get<std::string>();
		default:
			if (data.contains("contentId"))
				return data["contentId"].get<std::string>();
			else
				std::cout << "Couldnt find Content ID" << std::endl;
			break;
		}
	}
	catch (std::exception& e)
	{
		std::cout << "getIdFromItem Exception: " << e.what() << std::endl;
	}
	return "";
}

std::vector<std::string> JsonProcessing::getRatingsFromItem(const json& data)
{
	std::vector<std::string> ret;
	try {
		switch (data["type"].get<SetType>())
		{
		case SetType::VIDEO:
		case SetType::SERIES:
			for (auto rating = 0; rating < data["ratings"].size(); rating++)
			{
				ret.push_back(data["ratings"][rating]["value"]);
			}
			break;
		case SetType::STANDARD_COLLECTION:
			//No rating displayed here. show NA
			ret.push_back("N/A");
			break;
		default:
			std::cout << "getRatingsFromItem -unexpected item type " << (int)data["type"].get<SetType>() << std::endl;
			break;
		}
	}
	catch (std::exception& e)
	{
		std::cout << "getRatingsFromItem Exception: " << e.what() << std::endl;
	}

	return ret;
}

std::vector<std::string> JsonProcessing::getReleaseDateFromItem(const json& data)
{
	std::vector<std::string> ret;
	try {
		switch (data["type"].get<SetType>())
		{
		case SetType::VIDEO:
		case SetType::SERIES:
			for (auto release = 0; release < data["releases"].size(); release++)
			{
				ret.push_back(data["releases"][release]["releaseDate"]);
			}
			break;
		case SetType::STANDARD_COLLECTION:
			//No release date displayed here. show NA
			ret.push_back("N/A");
			break;
		default:
			std::cout << "getReleaseDateFromItem -unexpected item type " << (int)data["type"].get<SetType>() << std::endl;
			break;
		}
	}
	catch (std::exception& e)
	{
		std::cout << "getRatingsFromItem Exception: " << e.what() << std::endl;
	}

	return ret;
}

bool JsonProcessing::isSetRef(const json& data)
{
	try
	{
		if (data["type"].get<SetType>() == SetType::REF)
			return true;
	}
	catch (std::exception& e)
	{
		std::cout << "isSetRef Exception: " << e.what() << std::endl;
	}
	return false;
}

json JsonProcessing::strToJson(const std::string data)
{
	return json::parse(data);
}
