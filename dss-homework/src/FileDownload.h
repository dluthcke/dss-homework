#pragma once
#include <string>
#include <vector>

class FileDownload
{
public:
	std::string DownloadUrlToString(const std::string url);

	std::vector<char> DownloadUrlToBin(const std::string url);

	bool DownloadUrlToFile(const std::string url, const std::string filepath);
};

