#pragma once
#include <string>
#include <json/json.hpp>

#include "JsonTypes.h"

// for convenience
using json = nlohmann::json;

class JsonProcessing
{
public:
	std::vector<json> getContainersFromHome(const std::string& data);

	std::vector<json> getItemsFromSet(const json& data);

	classText getTextFromItem(const json& data);

	typedef std::map<std::string, std::string> imageType_t;
	typedef std::map<std::string, imageType_t> jsonImagesType_t;
	jsonImagesType_t getImageFromItem(const json& data);

	std::string getContentIdFromItem(const json& data);
	
	std::vector <std::string> getRatingsFromItem(const json& data);
	std::vector <std::string> getReleaseDateFromItem(const json& data);

	bool isSetRef(const json& data);

	json strToJson(const std::string data);

private:

};

