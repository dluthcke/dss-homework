#include "Management.h"
#include <filesystem>
#include <iostream>
#include <windows.h>
#include <regex>

Management::Management(JsonProcessing& jp, FileDownload& fd, Rendering& render, std::string fileFolder)
	: m_RerenderSem(1)
	, m_terminateCalled(false)
	, m_renderLandingPage(false)
	, m_JsonProc(jp)
	, m_FileDownload(fd)
	, m_Render(render)
	, m_fileDir(fileFolder)
{
	using namespace std::placeholders;
	m_Render.registerColIncrementCallback(std::bind(&Management::ColIncrementHandler, this, _1, _2));
	m_Render.registerRowIncrementCallback(std::bind(&Management::RowIncrementHandler, this, _1));
	m_Render.registerActionCallback(std::bind(&Management::ActionHandler, this, _1, _2));
	m_Render.registerBackCallback(std::bind(&Management::BackHandler, this));
	m_Render.registerExitCallback(std::bind(&Management::Exithandler, this));

	m_PageVisualMap.insert(std::pair< HomePageItems_t, unsigned int>(HomePageItems_t::CONTAINER, 1));
	m_PageVisualMap.insert(std::pair< HomePageItems_t, unsigned int>(HomePageItems_t::ROW_1, 1));
	m_PageVisualMap.insert(std::pair< HomePageItems_t, unsigned int>(HomePageItems_t::ROW_2, 1));
	m_PageVisualMap.insert(std::pair< HomePageItems_t, unsigned int>(HomePageItems_t::ROW_3, 1));
	m_PageVisualMap.insert(std::pair< HomePageItems_t, unsigned int>(HomePageItems_t::ROW_4, 1));
}

Management::~Management()
{
}

void Management::run()
{
	//Download the homepage json
	std::string homeUrl("https://cd-static.bamgrid.com/dp-117731241344/home.json");
	std::string result = m_FileDownload.DownloadUrlToString(homeUrl);

	//Parse the json to extract the containers and attached items
	m_ContainerList = m_JsonProc.getContainersFromHome(result);

	while (!m_Render.getInitComplete())
	{
		Sleep(1000);
	}

	m_Render.setTileHeight(125);
	m_Render.setTileWidth(125 * 1.78);

	while (!m_terminateCalled)
	{	
		if (!m_renderLandingPage)
		{
			loadVisibleTiles();
		}
		m_RerenderSem.acquire();
	}
}

void Management::ColIncrementHandler(const int row, const bool decrement)
{
	//Only decrement if the visible row is greater than 1;
	auto visualColumn = m_PageVisualMap.at((HomePageItems_t)row);
	auto origValue = visualColumn;

	if (decrement)
	{
		if (visualColumn > 1)
			visualColumn--;
	}
	else
	{
		// The max starting visual column should be the # of 
		// elements - # of columns
		if (visualColumn <= (m_PageTotalsMap.at((HomePageItems_t)row) - m_Render.Max_Columns))
			visualColumn++;
	}

	if (visualColumn != origValue)
	{
		if (m_PageVisualMap.contains((HomePageItems_t)row))
			m_PageVisualMap.erase((HomePageItems_t)row);

		m_PageVisualMap.insert(std::pair<HomePageItems_t, unsigned int>((HomePageItems_t)row, visualColumn));

		//Kick off a row thread, need to adjust row because thread expects
		// a 0-3 index and we return 1-4
		std::thread rowThread(&Management::updateRow, this, (row-1));
		rowThread.detach();
	}
}

void Management::RowIncrementHandler(const bool decrement)
{
	auto firstRowVisual = m_PageVisualMap.at(HomePageItems_t::CONTAINER);
	auto valueChanged = false;

	//TODO: when shifting down, need to move all row positions up 1
	//TODO; when shifiting up, need to move all row positions down 1

	if (decrement)
	{
		if (firstRowVisual > 1)
		{
			firstRowVisual--;
			//After decrementing, we need to shift the rows down 
			// with the new row starting at 1
			// TODO: add in row reset code
			valueChanged = true;
		}
	}
	else
	{
		if (firstRowVisual <= (m_PageTotalsMap.at(HomePageItems_t::CONTAINER) - m_Render.Max_Rows))
		{
			firstRowVisual++;
			//After incrementing, we need to shift the rows up 
			// with the new row starting at 1
			// TODO: add in row reset code
			valueChanged = true;
		}
	}

	//Only re-render if a value changed
	if (valueChanged)
	{
		if (m_PageVisualMap.contains(HomePageItems_t::CONTAINER))
			m_PageVisualMap.erase(HomePageItems_t::CONTAINER);

		m_PageVisualMap.insert(std::pair<HomePageItems_t, unsigned int>(HomePageItems_t::CONTAINER, firstRowVisual));

		m_RerenderSem.release();
	}
}

void Management::ActionHandler(const unsigned int row, const unsigned int column)
{
	if (!m_renderLandingPage)
	{
		m_renderLandingPage = true;
		m_Render.clearScreen();
		//drawItemLandingPage( row, column);
		std::thread actionThread(&Management::drawItemLandingPage, this, row, column);
		actionThread.detach();

	}
}

void Management::BackHandler()
{
	if (m_renderLandingPage)
	{
		m_renderLandingPage = false;
		m_Render.clearScreen();
		m_RerenderSem.release();
	}
}

void Management::Exithandler()
{
	m_terminateCalled = true;
	m_RerenderSem.release();
}

bool Management::loadVisibleTiles()
{
	//Save off the total number of containers
	if (m_PageTotalsMap.contains(HomePageItems_t::CONTAINER))
		m_PageTotalsMap.erase(HomePageItems_t::CONTAINER);
	m_PageTotalsMap.insert(std::pair<HomePageItems_t, unsigned int>(HomePageItems_t::CONTAINER, m_ContainerList.size()));

	//the first container + 3 more are going to be visible
	for (auto visibleRowIndex = 0; visibleRowIndex < 4; visibleRowIndex++)
	{
		//updateRow(visibleRowIndex);
		std::thread rowThread(&Management::updateRow, this, visibleRowIndex);
		rowThread.detach();
	}
	return false;
}

void Management::updateRow(const int rowIndex)
{
	//setup row title text
	int currentRow = rowIndex + m_PageVisualMap.at(HomePageItems_t::CONTAINER);
	json currentContainer = m_ContainerList.at(currentRow - 1);
	json items;

	m_Render.createRowTitle(m_JsonProc.getTextFromItem(currentContainer).content, rowIndex + 1);

	//Get the items out of the current container
	if (!m_JsonProc.isSetRef(currentContainer))
	{
		items = m_JsonProc.getItemsFromSet(currentContainer);
	}
	else
	{
		//check to see if we have already downloaded this file before redownloading
		std::string refId = currentContainer["refId"].get<std::string>();

		//figure out the correct ref type for the downloaded file
			//TODO: the BecauseYouSet type is just a curatedSet in the file, find out a better way to do this
		std::string refType =
			(currentContainer["refType"].get<std::string>().compare("BecauseYouSet"))
			? currentContainer["refType"].get<std::string>() : "CuratedSet";

		if (!m_refIdMap.contains(refId))
		{
			//Download the secondary json first before we get the images
			std::string refUrl = "https://cd-static.bamgrid.com/dp-117731241344/sets/" + refId + ".json";

			std::string ref = m_FileDownload.DownloadUrlToString(refUrl);

			//the ref file will have different selection depending on type
			json refJson = m_JsonProc.strToJson(ref);

			m_refIdMap.insert(std::pair<std::string, json>(refId, refJson));
		}

		items = m_JsonProc.getItemsFromSet(m_refIdMap.at(refId)["data"][refType]);
	}


	//Show a right arrow on this row if there are still more items 
	//and a left arrow if the first isn't displayed
	//Also setup the page layouts here
	bool moreArrowNeeded = false;
	bool lessArrowNeeded = false;

	auto visibleIndex = rowIndex + 1;
	lessArrowNeeded = (m_PageVisualMap.at((HomePageItems_t)visibleIndex) != 1) ? true : false;
	moreArrowNeeded = (m_PageVisualMap.at((HomePageItems_t)visibleIndex) + 4 < items.size()) ? true : false;

	if (m_PageTotalsMap.contains((HomePageItems_t)visibleIndex))
		m_PageTotalsMap.erase((HomePageItems_t)visibleIndex);

	m_PageTotalsMap.insert(std::pair<HomePageItems_t, unsigned int>((HomePageItems_t)visibleIndex, items.size()));

	if (lessArrowNeeded)
		m_Render.createLessArrow(visibleIndex);
	else
		m_Render.deleteLessArrow(visibleIndex);

	if (moreArrowNeeded)
		m_Render.createMoreArrow(visibleIndex);
	else
		m_Render.deleteMoreArrow(visibleIndex);

	//parse out visible items in this row
	for (auto tileColumnIndex = 0; tileColumnIndex < 5; tileColumnIndex++)
	{
		auto thisColumn = m_PageVisualMap.at((HomePageItems_t)visibleIndex) + tileColumnIndex;

		//setup a loading tile and then set off a thread to download and display new tile.
		m_Render.createTile("imgs/loading.jpg", (tileColumnIndex + 1), (rowIndex + 1));
		std::thread itemThread(&Management::downloadAndDisplayTitle, this, items, (currentRow - 1), (thisColumn - 1), visibleIndex, (tileColumnIndex + 1));
		itemThread.detach();
	}

}

void Management::downloadAndDisplayTitle(const json items, const unsigned int containerIndex, const unsigned int itemIndex, const unsigned int row, const unsigned int column)
{
	//Extract the title image URL and content ID
	JsonProcessing::jsonImagesType_t images = m_JsonProc.getImageFromItem(items.at(itemIndex));
	std::string imageUrl(images["tile"]["1.78"]);
	std::string contentId(m_JsonProc.getContentIdFromItem(items.at(itemIndex)));

	try
	{
		//Creating the content ID directory
		auto createdDir = std::filesystem::create_directory(m_fileDir + "/" + contentId);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	
	std::string imageFileName(m_fileDir + "/" + contentId + "/title.jpg");
	//Check if a file exists before downloading (C++17)
	if (!std::filesystem::exists(imageFileName))
	{
		//Download tile image to a file, SDL doesn't seem to support loading from memory?
		m_FileDownload.DownloadUrlToFile(imageUrl, imageFileName);
	}
	
	//Create tile and text associated with this title.
	m_Render.createTile(m_fileDir + "/" + contentId + "/title.jpg", column, row);
	m_Render.createTileText(m_JsonProc.getTextFromItem(items.at(itemIndex)).content, column, row);
}

void Management::drawItemLandingPage(const int row, const int column)
{
	//find the correct item data
	json items;
	auto currentContainer = m_ContainerList.at((row - 1) + (m_PageVisualMap.at(HomePageItems_t::CONTAINER)-1));
	if (!m_JsonProc.isSetRef(currentContainer))
	{
		items = m_JsonProc.getItemsFromSet(currentContainer);
	}
	else
	{
		//check to see if we have already downloaded this file before redownloading
		std::string refId = currentContainer["refId"].get<std::string>();

		std::string refType =
			(currentContainer["refType"].get<std::string>().compare("BecauseYouSet"))
			? currentContainer["refType"].get<std::string>() : "CuratedSet";

		if (!m_refIdMap.contains(refId))
		{
			//Download the secondary json first before we get the images
			std::string refUrl = "https://cd-static.bamgrid.com/dp-117731241344/sets/" + refId + ".json";

			std::string ref = m_FileDownload.DownloadUrlToString(refUrl);

			//the ref file will have different selection depending on type
			json refJson = m_JsonProc.strToJson(ref);

			m_refIdMap.insert(std::pair<std::string, json>(refId, refJson));
		}

		items = m_JsonProc.getItemsFromSet(m_refIdMap.at(refId)["data"][refType]);
	}

	json item = items.at((column - 1) + (m_PageVisualMap.at((HomePageItems_t)row)-1));

	std::string title(m_JsonProc.getTextFromItem(item).content);

	std::string contentId(m_JsonProc.getContentIdFromItem(item));
	JsonProcessing::jsonImagesType_t images = m_JsonProc.getImageFromItem(item);

	//start with the background
	std::string backgroundImageUrl;
	if(images.contains("background") && images["background"].contains("1.78"))
		backgroundImageUrl = images["background"]["1.78"];
	else
	{
		//check to see if we have a hero_collection at the right size
		// this looks like it is only relevant to standardCollections
		if (images.contains("hero_collection"))
		{
			//check if its in the right aspect ratio, otherwise just grab the first
			if (images["hero_collection"].contains("1.78"))
				backgroundImageUrl = images["hero_collection"]["1.78"];
			else
				backgroundImageUrl = images["hero_collection"].begin()->second;
		}
	}

	//Modify the background url so it gets the right size
	//TODO: need to get dynamic window width
	backgroundImageUrl = std::regex_replace(backgroundImageUrl, std::regex("width=500"), "width=1600");

	std::string backgroundFileName(m_fileDir + "/" + contentId + "/background.jpg");
	//Check if a file exists before downloading (C++17)
	if (!std::filesystem::exists(backgroundFileName))
	{
		//Download tile image to a file, SDL doesn't seem to support loading from memory?
		m_FileDownload.DownloadUrlToFile(backgroundImageUrl, backgroundFileName);
	}

	//now do the poster
	std::string posterImageUrl;
	//check if the right aspect ratio exists, 
	// if it doesnt just grab the first one
	if (images["tile"].contains("0.75"))
		posterImageUrl = images["tile"]["0.75"];
	else
		posterImageUrl = images["tile"].begin()->second;

	std::string posterFileName(m_fileDir + "/" + contentId + "/poster.jpg");
	//Check if a file exists before downloading (C++17)
	if (!std::filesystem::exists(posterFileName))
	{
		//Download tile image to a file, SDL doesn't seem to support loading from memory?
		m_FileDownload.DownloadUrlToFile(posterImageUrl, posterFileName);
	}

	//get the rating
	std::vector<std::string> ratings = m_JsonProc.getRatingsFromItem(item);
	std::string rating(ratings.at(0));	

	//get the release date
	std::vector<std::string> releaseDates = m_JsonProc.getReleaseDateFromItem(item);
	std::string releaseDate(releaseDates[0]);

	m_Render.renderInfoPage(title, backgroundFileName, posterFileName, rating, releaseDate);
}



