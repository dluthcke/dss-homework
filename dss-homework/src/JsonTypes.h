#include <json/json.hpp>

enum class ContainerType
{
	CT_SHELF,
	CT_CURATED_SET,
	CT_STANDARD_COLLECTION,
};

enum class SetType
{
	BECAUSE_YOU,
	CURATED,
	TRENDING,
	PERSONALIZED_CURATED,
	REF,
	STANDARD_COLLECTION,
	SERIES,
	VIDEO,
};

struct container
{
	nlohmann::json setData;
	ContainerType type;
};

//------------------------------------------------
// Common Structs
//------------------------------------------------
struct classText
{
	std::string content;
	std::string language;
	std::string SourceEntity;
};