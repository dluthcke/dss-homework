#include "Rendering.h"
#include <iostream>
#include <SDL2/SDL_ttf.h>

#define WINDOW_WIDTH    1600
#define WINDOW_HEIGHT   900

Rendering::Rendering()
    : m_win(0)
    , m_rend(nullptr)
    , m_initComplete(false)
{
    m_TileWidth = 0;
    m_TileHeight = 0;
}

void Rendering::runRender()
{
    SDL_SetMainReady();
    // returns zero on success else non-zero
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        printf("error initializing SDL: %s\n", SDL_GetError());
    }

    m_win = SDL_CreateWindow("DSS-Homework", // creates a window
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        WINDOW_WIDTH,
        WINDOW_HEIGHT, 0);

    // creates a renderer to render our images
    m_rend = SDL_CreateRenderer(m_win, -1, SDL_RENDERER_ACCELERATED);

    TTF_Init();
    IMG_Init(0);

    //The initialization completed
    m_initComplete = true;

    // controls animation loop
    int close = 0;

    //If we are on the homepage, this is the selected row/column
    unsigned int selectedRow = 1;
    unsigned int selectedColumn = 1;

    // animation loop
    while (!close) {
        
        SDL_Event event;

        // Events management
        while (SDL_PollEvent(&event)) 
        {
            switch (event.type) {

            case SDL_QUIT:
                // handling of close button
                close = 1;
                m_exitCallback();
                break;
            case SDL_KEYDOWN:
                // keyboard API for key pressed
                switch (event.key.keysym.scancode) {
                case SDL_SCANCODE_UP:
                    if (selectedRow > 1)
                        selectedRow--;
                    else
                        m_rowIncCallback(true);
                    break;
                case SDL_SCANCODE_LEFT:
                    if (selectedColumn > 1)
                        selectedColumn--;
                    else
                        m_columnIncCallback(selectedRow, true);
                    break;
                case SDL_SCANCODE_DOWN:
                    if (selectedRow < Max_Rows)
                        selectedRow++;
                    else
                        m_rowIncCallback(false);
                    break;
                case SDL_SCANCODE_RIGHT:
                    if (selectedColumn < Max_Columns)
                        selectedColumn++;
                    else
                        m_columnIncCallback(selectedRow, false);
                    break;
                case SDL_SCANCODE_RETURN:
                    //action on current selected row and column
                    m_actionCallback(selectedRow, selectedColumn);
                    break;
                case SDL_SCANCODE_BACKSPACE:
                    //send a back action to go to a previous screen
                    m_backCallback();
                    break;
                default:
                    break;
                }
            }
        }
        m_renderMutex.lock();
        // clears the screen
        SDL_RenderClear(m_rend);

        for (auto itTex = m_tileMap.begin(); itTex != m_tileMap.end(); itTex++)
        {
            auto renderData = (*itTex).second;
            if ((renderData.columnPos == selectedColumn)
                && (renderData.rowPos == selectedRow))
            {
                //zoom in on the selected tile
                SDL_Rect position;

                SDL_QueryTexture(renderData.texture, NULL, NULL, &position.w, &position.h);

                // adjust height and width of our image box.
                const int scalePixels = 30;
                position.w = renderData.position.w + scalePixels;
                position.h = renderData.position.h + scalePixels;
                position.x = renderData.position.x - (scalePixels/2);
                position.y = renderData.position.y - (scalePixels/2);

                SDL_RenderCopy(m_rend, renderData.texture, NULL, &position);
            }
            else
            {
                SDL_RenderCopy(m_rend, renderData.texture, NULL, &renderData.position);
            }
        }

        // triggers the double buffers
        // for multiple rendering
        SDL_RenderPresent(m_rend);

        m_renderMutex.unlock();

        // calculates to 60 fps
        SDL_Delay(1000 / 60);
    }


    // destroy all textures
    for (auto itSurface = m_tileMap.begin(); itSurface != m_tileMap.end(); itSurface++)
    {
        SDL_DestroyTexture((*itSurface).second.texture);
    }

    // destroy renderer
    SDL_DestroyRenderer(m_rend);

    // destroy window
    SDL_DestroyWindow(m_win);

    // close SDL
    SDL_Quit();
}

int Rendering::createTile(const std::string imgFile, const unsigned int column, const unsigned int row)
{
    if (m_initComplete)
    {
        if (column <= Max_Columns
            && row <= Max_Rows)
        {
            m_renderMutex.lock();

            TileData data;

            int buttonPositionX = ((WINDOW_WIDTH * (column-1)) / (Max_Columns+1)) + ((m_TileWidth/ 5) * column) + 25;
            int buttonPositionY = ((WINDOW_HEIGHT * (row-1)) / (Max_Rows+1)) + ((m_TileHeight/4) * row) + 20;

            // creates a surface to load an image into the main memory
            SDL_Surface* surface = IMG_Load(imgFile.c_str());

            if (!surface)
            {
                printf("Couldn't create surface - %s\n", SDL_GetError());
                m_renderMutex.unlock();
                return -1;
            }

            // loads image to our graphics hardware memory.
            data.texture = SDL_CreateTextureFromSurface(m_rend, surface);

            // clears main-memory
            SDL_FreeSurface(surface);

            // connects our texture with dest to control position
            SDL_QueryTexture(data.texture, NULL, NULL, &data.position.w, &data.position.h);

            // adjust height and width of our image box.
            data.position.w = m_TileWidth;
            data.position.h = m_TileHeight;
            data.position.x = buttonPositionX;
            data.position.y = buttonPositionY;

            //Keep track of where this is being put on the screen
            data.columnPos = column;
            data.rowPos = row;

            TILE_POSITIONS position = columnRowToEnum(row, column);

            if (m_tileMap.contains(position))
            {
                SDL_DestroyTexture(m_tileMap.at(position).texture);
                m_tileMap.erase(position);
            }

            m_tileMap.insert(std::pair<TILE_POSITIONS, TileData>(position,data));

            m_renderMutex.unlock();
        }
        else
        {
            printf("unable to create tile, out of bounds\n");
            return -1;
        }
    }
    else
    {
        printf("Unable to create tile");
        return -1;
    }

    return 0;
}

int Rendering::createTileText(const std::string title, const unsigned int column, const unsigned int row)
{
    if (m_initComplete)
    {
        if (column <= Max_Columns
            && row <= Max_Rows)
        {
            m_renderMutex.lock();
            int buttonPositionX = ((WINDOW_WIDTH * (column - 1)) / (Max_Columns + 1)) + ((m_TileWidth / 5) * column) + 25;
            int buttonPositionY = ((WINDOW_HEIGHT * (row - 1)) / (Max_Rows + 1)) + ((m_TileHeight / 4) * row) + 35;

            //Create the text for this tile
            TileData messageData;
            TTF_Font* Sans = TTF_OpenFont("imgs/Sans.ttf", 12);
            SDL_Color White = { 255, 255, 255 };
            SDL_Surface* messageSurface =
                TTF_RenderText_Solid(Sans, title.c_str(), White);

            messageData.texture = SDL_CreateTextureFromSurface(m_rend, messageSurface);

            SDL_FreeSurface(messageSurface);

            // connects our texture with dest to control position
            SDL_QueryTexture(messageData.texture, NULL, NULL, &messageData.position.w, &messageData.position.h);

            messageData.position.x = buttonPositionX;
            messageData.position.y = buttonPositionY + m_TileHeight;


            TILE_POSITIONS position = columnRowToEnum(row, column, true);

            if (m_tileMap.contains(position))
            {
                SDL_DestroyTexture(m_tileMap.at(position).texture);
                m_tileMap.erase(position);
            }

            m_tileMap.insert(std::pair<TILE_POSITIONS, TileData>(position, messageData));

            m_renderMutex.unlock();
        }
        else
        {
            printf("unable to create tile text, out of bounds\n");
            return -1;
        }
    }
    else
    {
        printf("Unable to create tile text: not initialized\n");
        return -1;
    }

    return 0;
}

int Rendering::createRowTitle(const std::string title, const unsigned int row)
{
    if (m_initComplete)
    {
        if (row <= Max_Rows)
        {
            m_renderMutex.lock();
            int buttonPositionY = ((WINDOW_HEIGHT * (row - 1)) / (Max_Rows + 1) + ((125 / 4) * row)) - 30;

            TileData messageData;
            TTF_Font* Sans = TTF_OpenFont("imgs/Sans.ttf", 20);
            SDL_Color White = { 255, 255, 255 };
            SDL_Surface* messageSurface =
                TTF_RenderText_Solid(Sans, title.c_str(), White);

            messageData.texture = SDL_CreateTextureFromSurface(m_rend, messageSurface);

            SDL_FreeSurface(messageSurface);

            // connects our texture with dest to control position
            SDL_QueryTexture(messageData.texture, NULL, NULL, &messageData.position.w, &messageData.position.h);

            // Set the position of the Category text
            messageData.position.x = 50;
            messageData.position.y = buttonPositionY;

            TILE_POSITIONS position = columnRowToEnum(row, 0);
            if (m_tileMap.contains(position))
            {
                SDL_DestroyTexture(m_tileMap.at(position).texture);
                m_tileMap.erase(position);
            }

            m_tileMap.insert(std::pair<TILE_POSITIONS, TileData>(columnRowToEnum(row, 0), messageData));

            m_renderMutex.unlock();
        }
    }
    return 0;
}

int Rendering::renderInfoPage(const std::string title, const std::string backgroundFile, const std::string posterFile, const std::string rating, const std::string releaseDate)
{
    SDL_Color White = { 255, 255, 255 };
    TTF_Font* Sans48 = TTF_OpenFont("imgs/Sans.ttf", 48);
    TTF_Font* Sans20 = TTF_OpenFont("imgs/Sans.ttf", 20);
    //--------------------
    //Setup the background
    //--------------------
    m_renderMutex.lock();
    TileData backGroundData;
    SDL_Surface* backgroundSurface = IMG_Load(backgroundFile.c_str());
    if (!backgroundSurface)
    {
        return -1;
        m_renderMutex.unlock();
    }
    // loads image to our graphics hardware memory.
    backGroundData.texture = SDL_CreateTextureFromSurface(m_rend, backgroundSurface);
    SDL_FreeSurface(backgroundSurface);
    SDL_QueryTexture(backGroundData.texture, NULL, NULL, &backGroundData.position.w, &backGroundData.position.h);
    m_renderMutex.unlock();

    backGroundData.position.w = WINDOW_WIDTH;
    backGroundData.position.h = WINDOW_HEIGHT;
    backGroundData.position.x = 0;
    backGroundData.position.y = 0;

    m_tileMap.insert(std::pair<TILE_POSITIONS, TileData>((TILE_POSITIONS)0, backGroundData));

    //--------------------
    //Setup the title text
    //--------------------
    TileData titleTextData;
    m_renderMutex.lock();
    SDL_Surface* titleTextSurface = TTF_RenderText_Solid(Sans48, title.c_str(), White);

    titleTextData.texture = SDL_CreateTextureFromSurface(m_rend, titleTextSurface);

    SDL_FreeSurface(titleTextSurface);
    SDL_QueryTexture(titleTextData.texture, NULL, NULL, &titleTextData.position.w, &titleTextData.position.h);
    m_renderMutex.unlock();

    // Set the position of the Category text
    titleTextData.position.x = 50;
    titleTextData.position.y = 50;

    m_tileMap.insert(std::pair<TILE_POSITIONS, TileData>((TILE_POSITIONS)1, titleTextData));

    //--------------------
    //Setup Poster
    //--------------------
    TileData posterData;
    m_renderMutex.lock();
    SDL_Surface* posterSurface = IMG_Load(posterFile.c_str());
    if (!posterSurface)
    {
        return -1;
    }

    // loads image to our graphics hardware memory.
    posterData.texture = SDL_CreateTextureFromSurface(m_rend, posterSurface);
    SDL_FreeSurface(posterSurface);
    SDL_QueryTexture(posterData.texture, NULL, NULL, &posterData.position.w, &posterData.position.h);
    m_renderMutex.unlock();

    posterData.position.x = 50;
    posterData.position.y = 150;

    m_tileMap.insert(std::pair<TILE_POSITIONS, TileData>((TILE_POSITIONS)2, posterData));


    //--------------------
    //Setup rating
    //--------------------
    TileData ratingData;
    
    std::string ratingString("Rating: " + rating);

    m_renderMutex.lock();
    SDL_Surface* ratingSurface = TTF_RenderText_Solid(Sans20, ratingString.c_str(), White);

    ratingData.texture = SDL_CreateTextureFromSurface(m_rend, ratingSurface);

    SDL_FreeSurface(ratingSurface);
    SDL_QueryTexture(ratingData.texture, NULL, NULL, &ratingData.position.w, &ratingData.position.h);
    m_renderMutex.unlock();

    // Set the position of the Category text
    ratingData.position.x = 600;
    ratingData.position.y = 200;

    m_tileMap.insert(std::pair<TILE_POSITIONS, TileData>((TILE_POSITIONS)3, ratingData));

    //--------------------
    //Setup release date
    //--------------------
    TileData releaseData;
    m_renderMutex.lock();
    std::string releaseString("Release Date: " + releaseDate);
    SDL_Surface* releaseSurface = TTF_RenderText_Solid(Sans20, releaseString.c_str(), White);

    releaseData.texture = SDL_CreateTextureFromSurface(m_rend, releaseSurface);

    SDL_FreeSurface(releaseSurface);
    SDL_QueryTexture(releaseData.texture, NULL, NULL, &releaseData.position.w, &releaseData.position.h);
    m_renderMutex.unlock();

    // Set the position of the Category text
    releaseData.position.x = 600;
    releaseData.position.y = 300;

    m_tileMap.insert(std::pair<TILE_POSITIONS, TileData>((TILE_POSITIONS)4, releaseData));

    return 0;
}

int Rendering::createMoreArrow(const unsigned int row)
{
    if (m_initComplete)
    {
        //Row has to be between 1 and max, not 0 indexed
        if (row >= 1 && row <= Max_Rows)
        {
            m_renderMutex.lock();
            TileData data;

            int arrowPositionX = WINDOW_WIDTH - 50;
            int arrowPositionY = ((WINDOW_HEIGHT * (row-1)) / (Max_Rows + 1)) + ((m_TileHeight / 4) * row) + 60;

            //// creates a surface to load an image into the main memory
            SDL_Surface* surface = IMG_Load("imgs/moreright.jpg");

            if (!surface)
            {
                printf("Couldn't create surface - %s\n", SDL_GetError());
                m_renderMutex.unlock();
                return -1;
            }

            // loads image to our graphics hardware memory.
            data.texture = SDL_CreateTextureFromSurface(m_rend, surface);

            // clears main-memory
            SDL_FreeSurface(surface);

            // connects our texture with dest to control position
            SDL_QueryTexture(data.texture, NULL, NULL, &data.position.w, &data.position.h);

            // adjust height and width of our image box.
            data.position.w = 50;
            data.position.h = 100;
            data.position.x = arrowPositionX;
            data.position.y = arrowPositionY;

            TILE_POSITIONS position;
            switch (row)
            {
            case 1:
                position = TILE_POSITIONS::ROW_1_MORE;
                break;
            case 2:
                position = TILE_POSITIONS::ROW_2_MORE;
                break;
            case 3:
                position = TILE_POSITIONS::ROW_3_MORE;
                break;
            case 4:
                position = TILE_POSITIONS::ROW_4_MORE;
                break;
            }

            if (m_tileMap.contains(position))
            {
                SDL_DestroyTexture(m_tileMap.at(position).texture);
                m_tileMap.erase(position);
            }

            m_tileMap.insert(std::pair<TILE_POSITIONS, TileData>(position, data));

            m_renderMutex.unlock();
        }
    }
    return 0;
}

int Rendering::createLessArrow(const unsigned int row)
{
    if (m_initComplete)
    {
        if (row <= Max_Rows)
        {
            m_renderMutex.lock();
            TileData data;

            int arrowPositionX = 0;
            int arrowPositionY = ((WINDOW_HEIGHT * (row-1)) / (Max_Rows + 1)) + ((m_TileHeight / 4) * row) + 60;

            // creates a surface to load an image into the main memory
            SDL_Surface* surface = IMG_Load("imgs/moreleft.jpg");

            if (!surface)
            {
                printf("Couldn't create surface - %s\n", SDL_GetError());
                m_renderMutex.unlock();
                return -1;
            }

            // loads image to our graphics hardware memory.
            data.texture = SDL_CreateTextureFromSurface(m_rend, surface);

            // clears main-memory
            SDL_FreeSurface(surface);

            // connects our texture with dest to control position
            SDL_QueryTexture(data.texture, NULL, NULL, &data.position.w, &data.position.h);

            // adjust height and width of our image box.
            data.position.w = 50;
            data.position.h = 100;
            data.position.x = arrowPositionX;
            data.position.y = arrowPositionY;

            TILE_POSITIONS position;
            switch (row)
            {
            case 1:
                position = TILE_POSITIONS::ROW_1_LESS;
                break;
            case 2:
                position = TILE_POSITIONS::ROW_2_LESS;
                break;
            case 3:
                position = TILE_POSITIONS::ROW_3_LESS;
                break;
            case 4:
                position = TILE_POSITIONS::ROW_4_LESS;
                break;
            }

            if (m_tileMap.contains(position))
            {
                SDL_DestroyTexture(m_tileMap.at(position).texture);
                m_tileMap.erase(position);
            }

            m_tileMap.insert(std::pair<TILE_POSITIONS, TileData>(position, data));

            m_renderMutex.unlock();
        }
    }
    return 0;
}

int Rendering::deleteMoreArrow(const unsigned int row)
{
    TILE_POSITIONS position;
    switch (row)
    {
    case 1:
        position = TILE_POSITIONS::ROW_1_MORE;
        break;
    case 2:
        position = TILE_POSITIONS::ROW_2_MORE;
        break;
    case 3:
        position = TILE_POSITIONS::ROW_3_MORE;
        break;
    case 4:
        position = TILE_POSITIONS::ROW_4_MORE;
        break;
    }

    if (m_tileMap.contains(position))
    {
        m_renderMutex.lock();
        SDL_DestroyTexture(m_tileMap.at(position).texture);
        m_renderMutex.unlock();
        m_tileMap.erase(position);
    }
    return 0;
}

int Rendering::deleteLessArrow(const unsigned int row)
{
    TILE_POSITIONS position;
    switch (row)
    {
    case 1:
        position = TILE_POSITIONS::ROW_1_LESS;
        break;
    case 2:
        position = TILE_POSITIONS::ROW_2_LESS;
        break;
    case 3:
        position = TILE_POSITIONS::ROW_3_LESS;
        break;
    case 4:
        position = TILE_POSITIONS::ROW_4_LESS;
        break;
    }

    if (m_tileMap.contains(position))
    {
        m_renderMutex.lock();
        SDL_DestroyTexture(m_tileMap.at(position).texture);
        m_renderMutex.unlock();
        m_tileMap.erase(position);
    }
    return 0;
}

void Rendering::clearScreen()
{
    //erase all textures to free up memory
    m_renderMutex.lock();
    for (auto it = m_tileMap.begin(); it != m_tileMap.end(); it++)
    {
        SDL_DestroyTexture((*it).second.texture);
    }
    m_renderMutex.unlock();

    m_tileMap.clear();
}

void Rendering::registerColIncrementCallback(TColCallback callback)
{
    m_columnIncCallback = callback;
}

void Rendering::registerRowIncrementCallback(TRowCallback callback)
{
    m_rowIncCallback = callback;
}

void Rendering::registerBackCallback(TBackCallback callback)
{
    m_backCallback = callback;
}

void Rendering::registerActionCallback(TActionCallback callback)
{
    m_actionCallback = callback;
}

void Rendering::registerExitCallback(TExitCallback callback)
{
        m_exitCallback = callback;
}


Rendering::TILE_POSITIONS Rendering::columnRowToEnum(const int row, const int column, const bool isText)
{
    TILE_POSITIONS position = TILE_POSITIONS::ROW_1_TITLE;
    //set to the row title and then offset based on enum.
    switch (row)
    {
    case 2:
        position = TILE_POSITIONS::ROW_2_TITLE;
        break;
    case 3:
        position = TILE_POSITIONS::ROW_3_TITLE;
        break;
    case 4:
        position = TILE_POSITIONS::ROW_4_TITLE;
        break;
    default:
        break;
    }

    //fall through cases to increment through the right number
    switch (column)
    {
    case 5:
        position = (TILE_POSITIONS)((int)position + 2);
    case 4:
        position = (TILE_POSITIONS)((int)position + 2);
    case 3:
        position = (TILE_POSITIONS)((int)position + 2);
    case 2:
        position = (TILE_POSITIONS)((int)position + 2);
    case 1:
        if(isText)
            position = (TILE_POSITIONS)((int)position + 2);
        else
            position = (TILE_POSITIONS)((int)position + 1);
    default:
        break;
    }

    return position;
}
